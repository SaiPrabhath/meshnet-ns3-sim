#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/bridge-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/network-module.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/mesh-helper.h"
#include "ns3/mesh-module.h"
#include "ns3/point-to-point-module.h"

#include <fstream>
#include <iostream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("meshmine");

uint32_t port{9};            //!< Receiving port number.
uint32_t bytesTotal{0};      //!< Total received bytes.
uint32_t packetsReceived{0}; //!< Total received packets.
uint32_t packetsSent{0};
double m_totalTime{100};
uint16_t m_packetSize{1024};
double m_packetInterval{0.1};
std::string m_CSVfileName{"meshmine.csv"}; //!< CSV filename.

void
TxTrace(Ptr<const Packet> p)
{
    NS_LOG_DEBUG("Sent " << p->GetSize() << " bytes");
    packetsSent++;
}

void
RxTrace(Ptr<const Packet> p)
{
    NS_LOG_DEBUG("Received " << p->GetSize() << " bytes");
    bytesTotal += p->GetSize();
    packetsReceived++;
}

class MeshNet
{
public:
    MeshNet();
    int Run();
    void CheckThroughput();

private:
    NodeContainer meshNodes;
    NetDeviceContainer meshDevices;
    Ipv4InterfaceContainer meshInterfaces;
    MeshHelper mesh;

};

MeshNet::MeshNet()
{}

int
main(int argc, char* argv[])
{
    MeshNet experiment;
    experiment.Run();

    return 0;
}

void
MeshNet::CheckThroughput()
{
    double kbs = (bytesTotal * 8.0) / 1000;
    bytesTotal = 0;

    std::ofstream out(m_CSVfileName, std::ios::app);

    out << (Simulator::Now()).GetSeconds() << "," << kbs << "," << packetsReceived << std::endl;

    out.close();
    //packetsReceived = 0;
    Simulator::Schedule(Seconds(1.0), &MeshNet::CheckThroughput, this);
}

int
MeshNet::Run()
{
    Packet::EnablePrinting();
    PacketMetadata::Enable();

    // blank out the last output file and write the column headers
    std::ofstream out(m_CSVfileName);
    out << "SimulationSecond,"
        << "ReceiveRate,"
        << "PacketsReceived," << std::endl;
    out.close();

    WifiHelper wifi;
    

    meshNodes.Create(3);

    YansWifiPhyHelper wifiPhy;
    YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default();
    wifiPhy.SetChannel(wifiChannel.Create());
    mesh = MeshHelper::Default();

    mesh.SetStackInstaller("ns3::Dot11sStack");
    /* If set to true different non-overlapping 20MHz frequency channels will be assigned
       to different mesh point interfaces. */
    mesh.SetSpreadInterfaceChannels(MeshHelper::SPREAD_CHANNELS);
    mesh.SetMacType("RandomStart", TimeValue(Seconds(0.1)));
    mesh.SetNumberOfInterfaces(1);
    meshDevices = mesh.Install(wifiPhy, meshNodes);
    mesh.AssignStreams(meshDevices, 0);


   // Set node positions manually
    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();
    positionAlloc->Add(Vector(50.0, 50.0, 0.0));
    positionAlloc->Add(Vector(90.0, 50.0, 0.0));
    positionAlloc->Add(Vector(50.0, 90.0, 0.0));
    mobility.SetPositionAllocator(positionAlloc);
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(meshNodes);

    wifiPhy.EnablePcapAll(std::string("mp"));
    AsciiTraceHelper ascii;
    wifiPhy.EnableAsciiAll(ascii.CreateFileStream("mesh.tr"));

    InternetStackHelper internetStack;
    internetStack.Install(meshNodes);

    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");
    meshInterfaces = address.Assign(meshDevices);

    UdpEchoServerHelper echoServer(port);
    uint16_t sinkNodeId = 2;
    ApplicationContainer serverApps = echoServer.Install(meshNodes.Get(sinkNodeId));
    serverApps.Start(Seconds(1.0));
    serverApps.Stop(Seconds(m_totalTime + 1));
    // ApplicationContainer serverApps2 = echoServer.Install(meshNodes.Get(1));
    // serverApps2.Start(Seconds(1.0));
    // serverApps2.Stop(Seconds(m_totalTime + 1));
    UdpEchoClientHelper echoClient(meshInterfaces.GetAddress(sinkNodeId), port);
    echoClient.SetAttribute("MaxPackets", UintegerValue(1000));
    echoClient.SetAttribute("Interval", TimeValue(Seconds(m_packetInterval)));
    echoClient.SetAttribute("PacketSize", UintegerValue(m_packetSize));
    ApplicationContainer clientApps = echoClient.Install(meshNodes.Get(0));
    Ptr<UdpEchoClient> app = clientApps.Get(0)->GetObject<UdpEchoClient>();
    app->TraceConnectWithoutContext("Tx", MakeCallback(&TxTrace));
    app->TraceConnectWithoutContext("Rx", MakeCallback(&RxTrace));
    clientApps.Start(Seconds(1.0));
    clientApps.Stop(Seconds(m_totalTime + 1.5));

    CheckThroughput();

    Simulator::Stop(Seconds(200.0));
    Simulator::Run();
    Simulator::Destroy();
    return 0;
}
